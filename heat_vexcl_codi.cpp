#include <iostream>
#include <vector>

#include <boost/program_options.hpp>

#include <codi.hpp>
#include <vexcl/vexcl.hpp>

namespace vex {
namespace sparse {

template <>
struct spmv_ops_impl<cl_double2, cl_double2> {
    // How to declare accumulator variable (s in s = sum(A_ij * x_j))
    static void decl_accum_var(backend::source_generator &src, const std::string &name)
    {
        src.new_line() << "double2 " << name << " = {0, 0};";
    }

    // How to append two values of type pack2:
    static void append(backend::source_generator &src,
            const std::string &sum, const std::string &val)
    {
        src.new_line() << sum << " = codi_add(" << sum << ", " << val << ");";
    }

    // How to compute row product. Specifically, how to do (s += A_ij * x_j)
    // kind of operation.
    static void append_product(backend::source_generator &src,
            const std::string &sum, const std::string &mat_val, const std::string &vec_val)
    {
        src.new_line() << sum << " = codi_add(" << sum << ", codi_mul(" << mat_val << ", " << vec_val << "));";
    }
};

} // namespace sparse
} // namespace vex

typedef vex::symbolic<double> sym_real;
typedef vex::symbolic<cl_double2> codi_real;
typedef codi::ActiveReal< codi::ForwardEvaluation<sym_real> > VexRealForward;

VEX_FUNCTION(double, get_value,    (cl_double2, p), return p.x; );
VEX_FUNCTION(double, get_gradient, (cl_double2, p), return p.y; );
VEX_FUNCTION(cl_double2, make_codi, (double, v)(double, g),
        double2 r = {v, g};
        return r;
        );

struct codi_add {
    template <class Real>
    Real operator()(Real a, Real b) {
        VexRealForward A(get_value(a), get_gradient(a));
        VexRealForward B(get_value(b), get_gradient(b));

        VexRealForward C = A + B;
        return make_codi(C.getValue(), C.getGradient());
    }
};

struct codi_mul {
    template <class Real>
    Real operator()(Real a, Real b) {
        VexRealForward A(get_value(a), get_gradient(a));
        VexRealForward B(get_value(b), get_gradient(b));

        VexRealForward C = A * B;
        return make_codi(C.getValue(), C.getGradient());
    }
};

std::string header() {
    vex::backend::source_generator src;

    get_value.define(src);
    get_gradient.define(src);
    make_codi.define(src);

    auto add = vex::generator::make_function<cl_double2(cl_double2, cl_double2)>(codi_add());
    auto mul = vex::generator::make_function<cl_double2(cl_double2, cl_double2)>(codi_mul());

    add.define(src, "codi_add");
    mul.define(src, "codi_mul");

    return src.str();
}

//---------------------------------------------------------------------------
void assemble(int n, double h, double alpha,
        std::vector<int>    &ptr,
        std::vector<int>    &col,
        std::vector<cl_double2> &val,
        std::vector<cl_double2> &r
        )
{
    ptr.clear(); ptr.reserve(n+1); ptr.push_back(0);
    col.clear(); ptr.reserve(n*3);
    val.clear(); val.reserve(n*3);

    r.clear(); r.resize(n, cl_double2{0.0, 1.0}); r.front() = r.back() = cl_double2{1/(h*h), 1.0};

    for(int i = 0; i < n; ++i) {
        if (i > 0) {
            col.push_back(i - 1);
            val.push_back(cl_double2{1 / (h*h), 1.0});
        }

        col.push_back(i);
        val.push_back(cl_double2{-2 / (h*h), 1.0});

        if (i + 1 < n) {
            col.push_back(i+1);
            val.push_back(cl_double2{1 / (h*h), 1.0});
        }

        ptr.push_back(col.size());
    }
}

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    //--- Options -----------------------------------------------------------
    namespace po = boost::program_options;

    po::options_description desc("Options");

    desc.add_options()
        ("help,h", "Show this help.")
        ("size,n",
         po::value<int>()->default_value(1024),
         "Problem size"
        )
        ("steps,m",
         po::value<int>()->default_value(1024),
         "Number of time steps"
        )
        ("alpha,a",
         po::value<double>()->default_value(1.0, "1.0"),
         "alpha coefficient"
        )
        ("dx,x",
         po::value<double>()->default_value(1e-2, "1e-2"),
         "Mesh spacing"
        )
        ("dt,t",
         po::value<double>()->default_value(1e-2, "1e-2"),
         "time step"
        )
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    int    n     = vm["size"].as<int>();
    int    m     = vm["steps"].as<int>();
    double h     = vm["dx"].as<double>();
    double tau   = vm["dt"].as<double>();
    double alpha = vm["alpha"].as<double>();

    //--- Initialize context ------------------------------------------------
    vex::Context ctx(vex::Filter::Env);
    std::cout << ctx << std::endl;

    vex::scoped_program_header hdr(ctx, header());

    //--- Asemble the problem -----------------------------------------------
    vex::profiler<> prof(ctx);
    prof.tic_cpu("assemble");
    std::vector<int> ptr, col;
    std::vector<cl_double2> val, r;

    assemble(n, h, alpha, ptr, col, val, r);
    prof.toc("assemble");

    vex::sparse::distributed< vex::sparse::matrix<cl_double2> > A(ctx, n, n, ptr, col, val);
    vex::vector<cl_double2> R(ctx, r), u(ctx, n), du(ctx, n);

    u = cl_double2{1.0, 1.0};

    auto add = vex::generator::make_function<cl_double2(cl_double2, cl_double2)>(codi_add());
    auto mul = vex::generator::make_function<cl_double2(cl_double2, cl_double2)>(codi_mul());

    prof.tic_cl("advance");
    for(int k = 0; k < m; ++k) {
        du = add(R, A * u);
        u = add(u, mul(cl_double2{tau, 0.0}, du));
    }
    prof.toc("advance");

    std::cout << static_cast<cl_double2>(u[0]) << std::endl;
    std::cout << static_cast<cl_double2>(u[42]) << std::endl;

    std::cout << prof << std::endl;
}
