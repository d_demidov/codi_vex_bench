#include <iostream>
#include <vector>

#include <boost/program_options.hpp>

#include <vexcl/profiler.hpp>

//---------------------------------------------------------------------------
void assemble(int n, double h, double alpha,
        std::vector<int>    &ptr,
        std::vector<int>    &col,
        std::vector<double> &val,
        std::vector<double> &r
        )
{
    ptr.clear(); ptr.reserve(n+1); ptr.push_back(0);
    col.clear(); ptr.reserve(n*3);
    val.clear(); val.reserve(n*3);

    r.clear(); r.resize(n, 0.0); r.front() = r.back() = 1/(h*h);

    for(int i = 0; i < n; ++i) {
        if (i > 0) {
            col.push_back(i - 1);
            val.push_back(1 / (h*h));
        }

        col.push_back(i);
        val.push_back(-2 / (h*h));

        if (i + 1 < n) {
            col.push_back(i+1);
            val.push_back(1 / (h*h));
        }

        ptr.push_back(col.size());
    }
}

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    //--- Options -----------------------------------------------------------
    namespace po = boost::program_options;

    po::options_description desc("Options");

    desc.add_options()
        ("help,h", "Show this help.")
        ("size,n",
         po::value<int>()->default_value(1024),
         "Problem size"
        )
        ("steps,m",
         po::value<int>()->default_value(1024),
         "Number of time steps"
        )
        ("alpha,a",
         po::value<double>()->default_value(1.0, "1.0"),
         "alpha coefficient"
        )
        ("dx,x",
         po::value<double>()->default_value(1e-2, "1e-2"),
         "Mesh spacing"
        )
        ("dt,t",
         po::value<double>()->default_value(1e-2, "1e-2"),
         "time step"
        )
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    int    n     = vm["size"].as<int>();
    int    m     = vm["steps"].as<int>();
    double h     = vm["dx"].as<double>();
    double tau   = vm["dt"].as<double>();
    double alpha = vm["alpha"].as<double>();

    //--- Asemble the problem -----------------------------------------------
    vex::profiler<> prof;
    prof.tic_cpu("assemble");
    std::vector<int> ptr, col;
    std::vector<double> val, r, u(n, 1.0), du(n);

    assemble(n, h, alpha, ptr, col, val, r);
    prof.toc("assemble");

    prof.tic_cpu("advance");
#pragma omp parallel
    {
        for(int k = 0; k < m; ++k) {
#pragma omp for
            for(int i = 0; i < n; ++i) {
                double s = r[i];

                for(int j = ptr[i], e = ptr[i+1]; j < e; ++j)
                    s += val[j] * u[col[j]];

                du[i] = s;
            }

#pragma omp for
            for(int i = 0; i < n; ++i)
                u[i] += tau * du[i];
        }
    }
    prof.toc("advance");

    std::cout << u[0] << std::endl;
    std::cout << u[42] << std::endl;

    std::cout << prof << std::endl;
}
